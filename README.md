# EDM supplementary files

This is the repository for the supplementary files for the Enhanced Dexterity Map (EDM). 

## change history

**2023 Oct 31:**<br>
Upload supplementary files for RA-L paper.

## content

`Supplementary_RA-L_EDM_20231031`<br>
supplementary files for RA-L paper. Further explanation on the algorithms used in this paper.
